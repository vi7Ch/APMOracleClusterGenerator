/**
* The MIT License (MIT)
* 
* Copyright (c) 2017 Viseth Chaing (Vi7)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/


/*
* # APMOracleClusterGenerator: Generate and deploy the APM info files to monitor clustered Oracle instances. 
* APM files build are based on RTSM info and Infra MP cluster discovery and CACIB publishing database 
*
* ### 07/10/2017: Version 0.1 - initial release - Viseth Chaing
* ### 07/18/2017: Version 0.2 - minor fixes to adapt to the OMi Scheduled Task policy execution environment - Viseth Chaing
* ### 08/02/2017: Version 0.3 - exit the generation when null value is encountered - Viseth Chaing
*
*/

//package vi7.monitoring

import org.slf4j.*
import groovy.util.logging.Slf4j
import ch.qos.logback.classic.*
import static ch.qos.logback.classic.Level.*
import groovy.time.*
import groovy.xml.*
import org.codehaus.groovy.runtime.StackTraceUtils
import java.util.jar.Manifest
import com.hp.ucmdb.api.*
import com.hp.ucmdb.api.topology.*
import com.hp.ucmdb.api.view.ViewService
import com.hp.ucmdb.api.view.result.ViewResult
import com.hp.ucmdb.api.view.result.ViewResultTreeNode
import com.hp.ucmdb.api.types.CI
import com.hp.ucmdb.api.types.TopologyCI
import org.yaml.snakeyaml.Yaml
import groovy.xml.MarkupBuilder
import org.apache.commons.io.FileUtils

@Slf4j
class APMOracleClusterGenerator {
	private String OMI_DPSPROTOCOL
	private String OMI_DPSHOST
	private Integer OMI_DPSPORT
	private String OMI_VIEWNAME
	private String OUTPUTAPM
	private String OS_SPECIFIC_DELM
	private String OMI_USER
	private String OMI_PWDENCRYPTED
	private String CMD_EXE
	private String TOPAZ_HOME
	private String FIND_STRING
	private String FILE_APMRTSMDATA = "APMRTSMData.yaml"
	private	String FILE_APMRTSMDATA_OLD = "APMRTSMData_old.yaml"
	private	String APMTEMPLATESCMDS_FILE = "APMTemplatesCmds.yaml"

	def timeSpent
	def today = new Date()

	String pathToConfigFile = System.getProperty("configFile")
	String CFG_FILE = pathToConfigFile
	ConfigObject cfg
	
	String WORKING_DIR = System.getProperty("workingDir")
	final String PWD = System.getProperty("pwd")
	
	
	APMOracleClusterGenerator() {			
		if (PWD)
			encrypt(PWD)
		else {
			WORKING_DIR = "${WORKING_DIR}\\"
			if (System.properties['os.name'].toLowerCase().contains('windows')) {
				OS_SPECIFIC_DELM = '\\' 
				CMD_EXE = ".bat" 
				FIND_STRING = "findstr"
				TOPAZ_HOME = "cmd /c echo %TOPAZ_HOME%"
			} else {
				OS_SPECIFIC_DELM = '/' 
				CMD_EXE = ".sh"
				FIND_STRING = "grep"
				TOPAZ_HOME = "echo $TOPAZ_HOME"
			}
			cfg = readConfigFile()
			OMI_DPSPROTOCOL = cfg.omi.DPSProtocol
			OMI_DPSHOST = cfg.omi.DPSHost
			OMI_DPSPORT = cfg.omi.DPSPort
			OMI_VIEWNAME = cfg.omi.viewName
			OMI_USER = cfg.omi.user
			OMI_PWDENCRYPTED = cfg.omi.pwdEncrypted
			OUTPUTAPM = "${WORKING_DIR}APM"
			def manifest = getManifestInfo()
			log.info "=== Initialization ${this?.class?.name}: Version: ${manifest?.getValue('Specification-Version')} / Author: ${manifest?.getValue('Created-By')} / Built-Date: ${manifest?.getValue('Built-Date')}"
			checkHACStatus(TOPAZ_HOME)
		}
	}


	String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter()
		PrintWriter pw = new PrintWriter(sw, true)
		t.printStackTrace(pw)
		pw.flush()
		sw.flush()
		return sw.toString()
	}


	// Retrieves the Manifest Info from the JAR file
	def getManifestInfo() {
		Class clazz = this.getClass()
		String className = clazz.getSimpleName() + ".class"
		String classPath = clazz.getResource(className).toString()
		// Class not from JAR
		if (!classPath.startsWith("jar")) { return null }
		String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) + "/META-INF/MANIFEST.MF"
		Manifest manifest = null
		try {
			manifest = new Manifest(new URL(manifestPath).openStream())
		} catch (Exception e) {
			StackTraceUtils.deepSanitize(e)
			log.warn "Manifest: ${getStackTrace(e)}"
		}
		return manifest.getMainAttributes()
	}

	
	def encrypt(pwd) {
		String encoded = pwd.bytes.encodeBase64().toString()
		System.exit(0)
	}
	
	
	def decrypt(pwd) {
		byte[] decoded = "$pwd".decodeBase64()
		def password = new String(decoded)		
		return "${password}"
	}
	
	
	ConfigObject readConfigFile() {
		try {
			def cfgFile
			if(CFG_FILE) {
				cfgFile = new File("${CFG_FILE}")
				if(!cfgFile.isFile()) {
					println "---> missing the configFile" 
					System.exit(0)
				}
			} else {
				// default to config.cfg if no config file pass has argument
				CFG_FILE = WORKING_DIR + 'config.cfg'
			}
			ConfigObject cfg = new ConfigSlurper().parse(new File(CFG_FILE).toURL())
			if (cfg) {
				log.trace "The configuration files: ${CFG_FILE} was read correctly"
				return cfg
			} else {
				log.error "Verify the content of the configuration file: ${CFG_FILE}"
				throw new RuntimeException("---> Verify the content of the configuration file: ${CFG_FILE}")
			}
		} catch(FileNotFoundException e) {
			log.error "The configuration file: ${CFG_FILE} was not found"
			println "The configuration file: ${CFG_FILE} was not found"
			System.exit(0)
			//throw new RuntimeException("---> The configuration file: ${CFG_FILE} was not found")
		} catch(Exception e) {
			log.error "Configuration file exception: ${getStackTrace(e)}"
			StackTraceUtils.deepSanitize(e)
			throw new RuntimeException("---> Configuration file exception:\n${getStackTrace(e)}")
		}
	}


	def oracleClusterInfo() {
		UcmdbServiceProvider provider
		try {
			if(OMI_DPSPROTOCOL == 'HTTPS') {
				//SSL  
				UcmdbServiceFactory.initSSL()
				provider = UcmdbServiceFactory.getServiceProvider(OMI_DPSPROTOCOL, OMI_DPSHOST, OMI_DPSPORT)
			} else {
				// no SSL
				provider = UcmdbServiceFactory.getServiceProvider(OMI_DPSHOST, OMI_DPSPORT)
			}
			def OMI_PWD = decrypt(OMI_PWDENCRYPTED)
			UcmdbService ucmdbService = provider.connect(
				provider.createCredentials(OMI_USER, OMI_PWD),
				provider.createClientContext("Groovy")
			)
			
			// Getting the query service
			TopologyQueryService queryService = ucmdbService.getTopologyQueryService()
			Topology topology = queryService.executeNamedQuery(OMI_VIEWNAME)
			Collection<TopologyCI> oraclesCIs = topology.getCIsByName("Oracle")
			Collection<TopologyCI> clusterRGCIs = topology.getCIsByName("ClusterResourceGroup")
			
			// Getting the view service
			ViewService viewService = ucmdbService.getViewService()
			if(viewService) {
				// Execute the view
				ViewResult result = viewService.executeView(OMI_VIEWNAME)
				// Get the roots of the result (the top most parents)
				List<ViewResultTreeNode> roots = result.roots()
				roots.each { ViewResultTreeNode root ->	
					recursiveList(root, oraclesCIs, clusterRGCIs)
				}
			} else {
				println "error connecting to RTSM"
				log.error "error connecting to RTSM"
			}	
		} catch (e) {
			println "Error connecting to RTSM - please see logfile for details"
			StackTraceUtils.deepSanitize(e)
			log.error "Configuration file exception: ${getStackTrace(e)}"	
			System.exit(0)
		}
	}
	

	void recursiveList(ViewResultTreeNode root, Collection<TopologyCI> oraclesCIs, Collection<TopologyCI> clusterRGCIs) {
		try {
			def clusterCILabel, instanceOracle, RGPackage, node
			def APMRTSMDataFile = new File(WORKING_DIR + FILE_APMRTSMDATA)	 
			if (root.ci().type ==~ /.*cluster/) { 
				clusterCILabel = root.ci().getPropertyValue('display_label')
				if(clusterCILabel) {
					APMRTSMDataFile << "-  ${clusterCILabel}:\n"
				} else {
					log.error "Failed to build the APMRTSMData.yaml - got null value. Check the RTSM for missing CI or relationship"
					System.exit(0)
				}
			}
			List<ViewResultTreeNode> children = root.children()
			if (children.size() > 0) {
				children.each { ViewResultTreeNode child ->
					if (child.ci().type ==~ /cluster_resource_group/) {
						for (CI  clusterRGCI : clusterRGCIs) {
							if(clusterRGCI.getPropertyValue('display_label') == child.ci().getPropertyValue('display_label')) {
								RGPackage = clusterRGCI.getPropertyValue('name')
							}
						}					
						if(RGPackage) {
							APMRTSMDataFile << "    -  instancePackage: ${RGPackage}\n"
						} else {
							log.error "Failed to build the APMRTSMData.yaml - got null value. Check the RTSM for missing CI or relationship"
							System.exit(0)
						}
					}
					if (child.ci().type ==~ /oracle/) {
						for (CI  oraclesCI : oraclesCIs) {
							if(oraclesCI.getPropertyValue('display_label') == child.ci().getPropertyValue('display_label')) {
								instanceOracle = oraclesCI.getPropertyValue('name')
							}
						}	
						if(instanceOracle) {
							//instanceOracle = child.ci().getPropertyValue('display_label').toString().split(/\(/)[0].trim()
							APMRTSMDataFile << "       instanceName: ${instanceOracle}\n"
							APMRTSMDataFile << "       nodes: \n"
						} else {
							log.error "Failed to build the APMRTSMData.yaml - got null value. Check the RTSM for missing CI or relationship"
							System.exit(0)
						}	
					}
					if (child.ci().type ==~ /node|unix|nt/) {
						node = child.ci().getPropertyValue('display_label')
						if(node) {
							APMRTSMDataFile << "         -  ${node}\n"
						} else {
							log.error "Failed to build the APMRTSMData.yaml - got null value. Check the RTSM for missing CI or relationship"
							System.exit(0)
						}	
					}
					if (child.children().size() > 0) { recursiveList(child, oraclesCIs, clusterRGCIs) }
				}
			}
		} catch(e) {
			println "Problem going through the RTSM view - please see logfile for details"
			StackTraceUtils.deepSanitize(e)
			log.error "RTSM view exception: ${getStackTrace(e)}"	
			System.exit(0)			
		}	
	}		
	
	
	def initializationCleanUp() {
		log.debug "-- WORKING_DIR : ${WORKING_DIR}"
		System.setProperty("user.dir", WORKING_DIR) 
		File fileAPMRTSMData = new File (WORKING_DIR + FILE_APMRTSMDATA)
		File fileAPMRTSMDataOld = new File (WORKING_DIR + FILE_APMRTSMDATA_OLD)
		File APMTemplatesCmdsFile = new File(WORKING_DIR + APMTEMPLATESCMDS_FILE)
		if (!APMTemplatesCmdsFile.exists()) {
			log.error "The required input file: APMTemplatesCmds.yaml was not found - exiting the program"
			println "The required input file: APMTemplatesCmds.yaml was not found - exiting the program"
			System.exit(0)			
		}		
		if (fileAPMRTSMDataOld.exists()) {
			boolean fileAPMRTSMDataOldDeleted =  fileAPMRTSMDataOld.delete()
			if ( fileAPMRTSMDataOldDeleted ) { 
				log.debug "APMRTSMData_old.yaml deleted" 
			}
			else { log.error "failed to delete APMRTSMData_old.yaml" }
		}
		if (fileAPMRTSMData.exists()) {
			log.debug "remaming APMRTSMData.yaml APMRTSMData_old.yaml for comparison purpose"
			fileAPMRTSMData.renameTo "${WORKING_DIR}APMRTSMData_old.yaml" 
		}
		File outputAPMDir = new File(OUTPUTAPM)
		if (!outputAPMDir.exists()) {
			outputAPMDir.mkdirs()
		}
	}

	
	/* 
	* Generate APMFiles
	*/
	def genAndPushAPMFiles() {
		//check that the APM data file exists or else exit
		File APMRTSMDataFile = new File(WORKING_DIR + FILE_APMRTSMDATA)
		File APMRTSMDataOldFile = new File(WORKING_DIR + FILE_APMRTSMDATA_OLD)
		if (!APMRTSMDataFile.exists()) {
			println "Failed to build the APMRTSMData.yaml - please see logfile for details"
			log.error "Failed to build the APMRTSMData.yaml"	
			System.exit(0)			
		} else {
			if(FileUtils.contentEquals(APMRTSMDataFile, APMRTSMDataOldFile)) {
				println "The data from the RTSM view has not changed - skipping this execution"
				log.info "The data from the RTSM view has not changed - skipping this execution"
				System.exit(0)
			}
		}	
		Yaml parser = new Yaml()
		List APMRTSMData = parser.load(APMRTSMDataFile.text)
		def APMTemplatesCmds = parser.load(("${WORKING_DIR}${APMTEMPLATESCMDS_FILE}" as File).text)
		APMRTSMData.each{ cluster ->
			cluster.each { cluAppKey, cluAppValue -> 
				//building the apminfo.xml file
				println cluAppKey
				new File(OUTPUTAPM+OS_SPECIFIC_DELM+"apminfo_${cluAppKey}.xml").withWriter { writer ->
					def xml = new groovy.xml.MarkupBuilder(writer)
					xml.mkp.xmlDeclaration(version: "1.0") 
					xml.APMClusterConfiguration {
						xml.Application {
							xml.Name(cluAppKey)
							cluAppValue.each { instanceData -> 
								xml."Instance" {
									xml.Name(instanceData."instanceName")
									xml.Package(instanceData."instancePackage")
								}	
							}
						}
					}
				}	
			}
			//building the apm.xml file
			cluster.each { cluAppKey, cluAppValue -> 
				log.info " === ${cluAppKey}"
				def fileAPMINFO, fileAPM, xml, ovDataDir, OS_NODE_SPECIFIC, sendDirAPMINFO, sendDirAPM
				new File(OUTPUTAPM+OS_SPECIFIC_DELM+"${cluAppKey}.apm.xml").withWriter { writer ->
					xml = new groovy.xml.MarkupBuilder(writer)
					xml.mkp.xmlDeclaration(version: "1.0") 
					xml.APMApplicationConfiguration(xmlns: "http://www.hp.com/OV/opcapm/app") {
						xml.Application {
							xml.Name(cluAppKey)
							APMTemplatesCmds."Templates".each { template -> 
								xml.Template(template)
							}
							xml.StartCommand(APMTemplatesCmds."StartCommand")
							xml.StopCommand(APMTemplatesCmds."StopCommand")
						}
					}
				}	
				cluAppValue[0]."nodes".each { node ->
					log.info "  |- $node"
					fileAPMINFO = new File(OUTPUTAPM+OS_SPECIFIC_DELM+"apminfo_${cluAppKey}.xml")
					fileAPM = new File(OUTPUTAPM+OS_SPECIFIC_DELM+"${cluAppKey}.apm.xml")
					ovDataDir = getOvDataDir(node)
					if (ovDataDir.contains('/')) {
						OS_NODE_SPECIFIC = '/'
					} else {
						OS_NODE_SPECIFIC = '\\'
					}
					sendDirAPMINFO = "${ovDataDir}${OS_NODE_SPECIFIC}conf${OS_NODE_SPECIFIC}conf${OS_NODE_SPECIFIC}"
					sendDirAPM = "${ovDataDir}${OS_NODE_SPECIFIC}bin${OS_NODE_SPECIFIC}instrumentation${OS_NODE_SPECIFIC}conf${OS_NODE_SPECIFIC}"
					sendAPMFiles(node, fileAPMINFO, sendDirAPMINFO, "-tn apminfo.xml")
					sendAPMFiles(node, fileAPM, sendDirAPM, "")
				}
			}	
		}
	}

	
	String executeCmd(def cmd) {
		log.debug cmd
		StringBuilder outputSendFiles = new StringBuilder()
		StringBuilder errorSendFiles = new StringBuilder()
		def proc = cmd.tokenize( '|' ).inject(null) { p, c ->
			if(p)
				p | c.execute()
			else
				c.execute()
		}
		proc.waitForProcessOutput(outputSendFiles,errorSendFiles)	
		log.debug outputSendFiles.toString().trim()
		log.debug errorSendFiles.toString().trim()
		return outputSendFiles.toString().trim()
	}

	
	String getOvDataDir(node) {
		def cmd, res
		cmd = "ovdeploy -cmd \"echo \$OvDataDir\" -host ${node} -ovrg server"
		res = executeCmd(cmd)
		log.debug "  |  ===> OA install dir on node ${node}: ${res}"
		return res
	}
	

	String checkHACStatus(def topazHome) {
		def cmd, topazHomeRes, res
		topazHomeRes = executeCmd(topazHome)
		log.debug "TOPAZ_HOME set to ${topazHomeRes}"
		cmd = "${topazHomeRes}${OS_SPECIFIC_DELM}JRE${OS_SPECIFIC_DELM}bin${OS_SPECIFIC_DELM}java  -Dmerc.home=${topazHomeRes} -Dtopaz.home=${topazHomeRes} -Dbus.clientProcessName=opr-support-utils -Dlog.folder=${topazHomeRes}${OS_SPECIFIC_DELM}opr${OS_SPECIFIC_DELM}support -client -cp \"${topazHomeRes}${OS_SPECIFIC_DELM}opr${OS_SPECIFIC_DELM}support${OS_SPECIFIC_DELM}lib${OS_SPECIFIC_DELM}opr-support-utils.jar;${topazHomeRes}${OS_SPECIFIC_DELM}opr${OS_SPECIFIC_DELM}lib${OS_SPECIFIC_DELM}cli${OS_SPECIFIC_DELM}opr-cli.jar;${topazHomeRes}${OS_SPECIFIC_DELM}lib${OS_SPECIFIC_DELM}*;${topazHomeRes}${OS_SPECIFIC_DELM}application-server${OS_SPECIFIC_DELM}bin${OS_SPECIFIC_DELM}client${OS_SPECIFIC_DELM}jboss-client.jar;D:${OS_SPECIFIC_DELM}Program Files${OS_SPECIFIC_DELM}HP${OS_SPECIFIC_DELM}HP BTO Software${OS_SPECIFIC_DELM}java${OS_SPECIFIC_DELM}*\" com.hp.opr.support.util.ToolMain -lhs | findstr opr-backend"
		res = executeCmd(cmd)
		log.debug "${cmd}: cmd output - ${res}"
		if (res ==~ /.*opr-backend.*/) {
			log.info "This DPS node opr-backend process is running. So running the program"
		} else {
			println "Stopping the program since another instance of it is probably running on the other DPS server"
			log.info "This DPS node opr-backend process is stopped. So stopping the program"
			System.exit(0)
		}
	}


	def sendAPMFiles(def node, def file, def sendDir, def sendOption) {
		def cmd, res
		cmd = "ovdeploy -upload -file \"${file}\" -td ${sendDir} ${sendOption} -host ${node} -ovrg server"
		//cmd = "ovdeploy -upload -file \"${file}\" -td /tmp ${sendOption} -host ${node} -ovrg server"
		res = executeCmd(cmd)
		log.info "  |  ===> sending ${file} file to node ${node}: ${res}"
	}	
	
	
	///////
	// MAIN
	///////
	static main(args) {
		def timeSpent 
		def main = new APMOracleClusterGenerator()
		main.initializationCleanUp()
		def timeStart = new Date()
		main.oracleClusterInfo()
		main.genAndPushAPMFiles()		
		def timeEnd = new Date()
		timeSpent = TimeCategory.minus(timeEnd,timeStart)
		log.info "Spent ${timeSpent}"
	}

}