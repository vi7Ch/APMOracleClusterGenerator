# APMOracleClusterGenerator: Generate and deploy the APM files to monitor clustered Oracle instances.
APM files are built based on RTSM data coming from OMi Infra MP with the cluster discovery and the publishing database

[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/vi7Ch/Test/LICENSE)
[![built with Groovy2.8](https://img.shields.io/badge/built%20with-Groovy-red.svg)](http://www.groovy-lang.org)
[![built with Java8](https://img.shields.io/badge/built%20with-Java-green.svg)](https://www.java.com/en/)

> **DISCLAIMER**
    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


![workflow](img/workflow.jpg)


### Version: 0.1 - initial release 07/10/2017 - Viseth Chaing
### Version: 0.2 - add logDir and minor fixes 07/13/2017 - Viseth Chaing

> **Deployment in OMi context** 
- Download the APMOracleClusterGenerator.jar file, the default config.cfg and the APMTemplatesCmds.yaml file. 
- Put them on OMi DPS management servers in a safe directory. 
- Modify according to your OMi environment the default config.cfg and the APMTemplatesCmds.yaml file. 
- Define a OMi scheduled Task policy that will execute the jar file. 
- Assign a category to this OMi policy and attach as instrumentation the jar file, the default config file and the APMTemplatesCmds.yaml (tose 3 files are required).
- Deploy this OMi policy on the OMi DPS management servers 
Note: The script will run only on the OMi DPS that host the opr-backend process.


### Manual Usage:
If no configFile is passed as an argument, the default named 'config.cfg' file in the current directory is used and a 'logs' directory is created with the logfile 

```bash
java -jar APMOracleClusterGenerator.jar
```

Though you can perform multiple executions with different configs by passing a config file

```bash
java -jar -DconfigFile=<path_to_my_configFile>/configFile1.cfg APMOracleClusterGenerator.jar
```

And you can pass as well the path of the logfile directory which is specific to the config file 

```bash
java -jar -DconfigFile=<path_to_my_configFile>/configFile1.cfg -DlogDir=<path_to_the_logDirForConfig1> APMOracleClusterGenerator.jar
```

To change to a less readable password

```bash
java -jar -Dpwd=<myNewPwd> APMOracleClusterGenerator.jar
``` 

### Note:
**Required INPUT files**
- A CONFIG file named 'config.cfg' if no config file is passed as an argument
- A YAML file named 'APMTemplateCmds.yaml' where you set the information about the Templates and the Command that will be used to construct the application apm file.

```
Templates:
    - <templateName1>
    - <templateName2>
    ...
    - <templateNameN>    
StartCommand: <the command to be execute on start>
StopCommand: <the command to be execute on stop>
```

**OUTPUT files**
- A YAML file that contains all the information to build the APM files with the below format:
```
- <clusterName>*
    -  instancePackage*: < instancePackageName>
       instanceName: < instanceNameName>
       node:
        -  <node1Name>
        -  <node2Name>
        -  <nodenName>
 
(*: 1-n items)
```

- The APM files found in the APM directory and that will be sent to the cluster nodes.
- Logfiles: this tool logs its events in a logfile with the below caracteristics: 
```bash
        - LogLevel: INFO, ERROR, DEBUG
        - LogSize: 10MB
        - LogRotate: 5 files
        - Directory of the logfile: 'logs' default to the current directory or a specific directory with '-DlogDir=<path_to_the_logDirForConfig1>'
        - Name: 'APMOracleClusterGenerator.log'

To turn debugging mode so that debugging lines will appear in the logfile:
java -Dapp.env=DEBUG APMOracleClusterGenerator.jar

```
A good practice would be to monitor this logfile and search for the ERROR pattern with the OMi logfile monitoring policy

The tool should be run through an OMi Scheduled Task policy. The frequency of execution will depends on the execution time and the refresh time interval of the RTSM data.

